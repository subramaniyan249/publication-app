import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormComponent } from '../components/form-component/form.component';
import { PublicationServiceService } from '../services/publication-service.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-publication-card',
  templateUrl: './publication-card.component.html',
  styleUrls: ['./publication-card.component.css']
})
export class PublicationCardComponent implements OnInit {

  @Input() publication;
  @Output() callPublicationsList = new EventEmitter();
  public active: boolean;
  constructor(public snackbar: MatSnackBar ,public dialog: MatDialog, private publicationService: PublicationServiceService) { }

  ngOnInit(): void {
    console.log(this.publication)
  }

  public editForm(publication): void {
    const dialogRef = this.dialog.open(FormComponent, {
      data: { publication: publication, edit: true }
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.callPublicationsList.emit(true)
    });
  }

  public delete(publication): void {
    this.publicationService.deletePublications(publication).subscribe(results => {
      this.callPublicationsList.emit(true)
      this.snackbar.open('One record deleted', 'ok', {
        duration: 2000
      });
    },
      error => {
        console.log(error)
      })
  }

  public likeOrUnlike(publication, like): void {
    this.publicationService.patchPublications(publication, like).subscribe(results => {
      this.callPublicationsList.emit(true);
      this.snackbar.open('Like/Unlike', 'ok', {
        duration: 2000
      });
    },
      error => {
        console.log(error)
      })
  }

}

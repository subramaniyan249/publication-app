import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class PublicationServiceService {
  baseURL: string = "http://localhost:3000"
  constructor(private http: HttpClient) { }

  public postPublications(formData) {
    return this.http.post<any>(`${this.baseURL}/publications`, formData);
  }

  public getPublications() {
    return this.http.get<any>(`${this.baseURL}/publications`)
  }

  public editPublications(formData) {
    return this.http.put<any>(`${this.baseURL}/publications/${formData.id}`, formData);
  }

  public deletePublications(formData) {
    return this.http.delete<any>(`${this.baseURL}/publications/${formData.id}`);
  }

  public patchPublications(formData, status) {
    return this.http.patch<any>(`${this.baseURL}/publications/${formData.id}`, {"like" : status});
  }
}

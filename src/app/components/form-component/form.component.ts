import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { PublicationServiceService } from '../../services/publication-service.service';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-form-component',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  publishForm: FormGroup;
  public editForm: boolean = false;

  constructor(public snackbar: MatSnackBar ,private fb: FormBuilder, public dialogRef: MatDialogRef<FormComponent>, private publicationService: PublicationServiceService, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.publishForm = this.fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required]
    });
    if (this.data && this.data.publication)
      this.initForm();
  }

  public createPublications(): void {
    if (!this.editForm) {
      this.publicationService.postPublications(this.publishForm.value).subscribe(results => {
        this.dialogRef.close();
        this.snackbar.open('New publications created', 'ok', {
          duration: 2000
        });
      },
        error => {
          console.log(error)
        })
    }
    else if (this.editForm) {
      this.publicationService.editPublications(this.publishForm.value).subscribe(results => {
       this.dialogRef.close();
       this.snackbar.open('publication edited', 'ok', {
        duration: 2000
      });
      },
        error => {
          console.log(error)
        })
    }
  }

  public cancel(): void {
    this.dialogRef.close()
  }

  public initForm() {
    this.publishForm.controls['title'].setValue(this.data.publication.title);
    this.publishForm.controls['description'].setValue(this.data.publication.description);
    this.publishForm.addControl('id', new FormControl(this.data.publication.id));
    if (this.data.edit)
      this.editForm = true;
  }

}

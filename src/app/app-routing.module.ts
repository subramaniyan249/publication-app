import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './components/form-component/form.component';
import { PublicationsComponent } from './publications/publications.component';

const routes: Routes = [
  {
    path: 'tasks',
    component: FormComponent
  },
  {
    path: '',
    component: PublicationsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { PublicationServiceService } from '../services/publication-service.service';
import { MatDialog } from '@angular/material/dialog';
import { FormComponent } from '../components/form-component/form.component';

@Component({
  selector: 'app-publications',
  templateUrl: './publications.component.html',
  styleUrls: ['./publications.component.css']
})
export class PublicationsComponent implements OnInit {

  public publications: any;
  public searchText: string;
  constructor(private publicationService: PublicationServiceService, public dialog: MatDialog,) { }

  ngOnInit(): void {
    this.getPublicationList()
  }

  public getPublicationList(): void {
    this.publicationService.getPublications().subscribe(results => {
      this.publications = results;
    },
    error => {
      console.log(error)
    })
  }

  openCreatePublicationForm() {
    const dialogRef = this.dialog.open(FormComponent);

    dialogRef.afterClosed().subscribe((result) => {
      this.getPublicationList();
      console.log(`Dialog result: ${result}`);
    });
  }

  getPublicationListEvent(event) {
    this.getPublicationList()
  }

}
